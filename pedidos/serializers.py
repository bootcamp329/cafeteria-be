from rest_framework import serializers
from .models import Pedido
import json

class PedidoSerializer(serializers.ModelSerializer):    
    class Meta:
        model = Pedido
        # fields = ['id', 'mesa', 'lista_productos']
        # fields = ['id', 'mesa', 'lista_productos','cantidad_productos']
        # fields = ['id', 'mesa', 'lista_productos','cantidad_productos', 'monto_total', 'listo']
        fields = ['id', 'mesa', 'lista_productos','cantidad_productos', 'monto_total', 'listo', 'servido']

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation['lista_productos'] = json.loads(representation['lista_productos'])
        representation['cantidad_productos'] = json.loads(representation['cantidad_productos'])
        return representation
        # for key in representation:
        #     return representation[key]
    
    